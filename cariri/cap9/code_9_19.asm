;=================================
;File: code_9_19
;=================================

.code
	LDA var1         ;este bloco realiza: (var1*2)*2 + var1.
	SHIFT esquerda
	SHIFT esquerda
	ADD var1

end:
	INT exit

.data
	;syscall exit
	exit: DD 25
	var1: DD 10
	esquerda: DD 1

.stack 1
