;=================================
;File: code_9_6
;=================================

.code
	LDA var1         ;este bloco realiza: NAND(var1, var2).
	NAND var2

end:
	INT exit

.data
	;syscall exit
	exit: DD 25
	var1: DD 1111111111111111b      ;int var1 = 0xffff.
	var2: DD 0000000000000000b      ;int var2 = 0.

.stack 1
